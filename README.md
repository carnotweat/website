# Gitea: Website

[![Build Status](https://drone.gitea.com/api/badges/gitea/website/status.svg)](https://drone.gitea.com/gitea/website)
[![Join the chat at https://img.shields.io/discord/322538954119184384.svg](https://img.shields.io/discord/322538954119184384.svg)](https://discord.gg/NsatcWJ)
[![](https://images.microbadger.com/badges/image/gitea/website.svg)](https://microbadger.com/images/gitea/website "Get your own image badge on microbadger.com")

## Hosting

This page is hosted on our infrastructure within Docker containers, it gets
automatically updated on every push to the `master` branch.

If you want to host this page on your own you can take our docker image
[gitea/website](https://hub.docker.com/r/gitea/website/).

## Install

This website uses the [Hugo](https://github.com/spf13/hugo) static site
generator. If you are planning to contribute you'll want to download and install
Hugo on your local machine.

The installation of Hugo is out of the scope of this document, so please take
the [official install instructions](https://gohugo.io/overview/installing/) to
get Hugo up and running.

## Development

To generate the website and serve it on [localhost:1313](http://localhost:1313)
just execute this command and stop it with `Ctrl+C`:

```
make server
```

When you are done with your changes just create a pull request, after merging
the pull request the website will be updated automatically.

## HOWTO manage sponsors

Gitea sponsors who donate the [minimal required amount for Gold](https://opencollective.com/gitea) have, during a year:

* Their logo is displayed on GitHub
* Their logo is on the gitea.io home page

This is done by:

* Adding their logo to the themes/gitea/static/images directory
* Adding a link with their logo to the home page at themes/gitea/layouts/home/index.html

The date of the addition must be recorded with a reminder to verify
the sponsorship has been renewed at the anniversary date. If not the links and logo can be removed.

Note that the `themes` directory is [located in another repository](https://gitea.com/gitea/theme) and that updates
are only deployed when the [website](https://gitea.com/gitea/website) repository is updated.

## Contributing

Fork -> Patch -> Push -> Pull Request

## Authors

* [Maintainers](https://gitea.com/org/gitea/members)
* [Contributors](https://github.com/go-gitea/website/graphs/contributors)

## License

This project is under the Apache-2.0 License. See the [LICENSE](LICENSE) file
for the full license text.

## Copyright

```
Copyright (c) 2020 The Gitea Authors <https://gitea.io>
```
