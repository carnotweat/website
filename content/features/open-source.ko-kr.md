---
date: "2016-11-08T16:00:00+02:00"
title: "오픈 소스"
weight: 10
toc: false
draft: false
---
<h3 class="subtitle is-3">
	<svg class="octicon octicon-code" viewBox="0 0 14 16" version="1.1" aria-hidden="true">
		<path fill-rule="evenodd" d="M9.5 3L8 4.5 11.5 8 8 11.5 9.5 13 14 8 9.5 3zm-5 0L0 8l4.5 5L6 11.5 2.5 8 6 4.5 4.5 3z"></path>
	</svg>
	오픈 소스
</h3>

모두 [GitHub](https://github.com/go-gitea/gitea/)에 있습니다!
이 프로젝트가 더 나아질 수 있도록 기여해주세요. 기여자가 되는 것에 주저하지 마세요!
